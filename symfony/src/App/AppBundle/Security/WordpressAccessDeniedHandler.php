<?php

namespace App\AppBundle\Security;

use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class WordpressAccessDeniedHandler implements AccessDeniedHandlerInterface, AuthenticationEntryPointInterface
{
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $url = '/wp-login.php?'.http_build_query(array(
            'redirect_to' => $request->getUri(),
            'reauth' => 0,
        ));

        return RedirectResponse::create($url, 302);
    }

    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $url = '/wp-login.php?'.http_build_query(array(
            'redirect_to' => $request->getUri(),
            'reauth' => 0,
        ));

        return RedirectResponse::create($url, 302);
    }
}
