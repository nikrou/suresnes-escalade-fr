<?php

namespace App\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment
 *
 * @ORM\Table(name="wp_app_payment")
 * @ORM\Entity
 */
class Payment {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var \stdClass
	 *
	 * @ORM\Column(name="subscription", type="object")
	 */
	private $subscription;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=30)
	 */
	private $type;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="reference", type="string", length=255)
	 */
	private $reference;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="amount", type="float")
	 */
	private $amount;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="due_date", type="datetime")
	 */
	private $dueDate;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_confirmed", type="boolean")
	 */
	private $isConfirmed;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="confirmed_at", type="datetime")
	 */
	private $confirmedAt;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="confirmed_by", type="string", length=255)
	 */
	private $confirmedBy;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set subscription
	 *
	 * @param \stdClass $subscription
	 * @return Payment
	 */
	public function setSubscription($subscription) {
		$this->subscription = $subscription;

		return $this;
	}

	/**
	 * Get subscription
	 *
	 * @return \stdClass
	 */
	public function getSubscription() {
		return $this->subscription;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 * @return Payment
	 */
	public function setType($type) {
		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Set reference
	 *
	 * @param string $reference
	 * @return Payment
	 */
	public function setReference($reference) {
		$this->reference = $reference;

		return $this;
	}

	/**
	 * Get reference
	 *
	 * @return string
	 */
	public function getReference() {
		return $this->reference;
	}

	/**
	 * Set amount
	 *
	 * @param float $amount
	 * @return Payment
	 */
	public function setAmount($amount) {
		$this->amount = $amount;

		return $this;
	}

	/**
	 * Get amount
	 *
	 * @return float
	 */
	public function getAmount() {
		return $this->amount;
	}

	/**
	 * Set dueDate
	 *
	 * @param \DateTime $dueDate
	 * @return Payment
	 */
	public function setDueDate($dueDate) {
		$this->dueDate = $dueDate;

		return $this;
	}

	/**
	 * Get dueDate
	 *
	 * @return \DateTime
	 */
	public function getDueDate() {
		return $this->dueDate;
	}

	/**
	 * Set isConfirmed
	 *
	 * @param boolean $isConfirmed
	 * @return Payment
	 */
	public function setIsConfirmed($isConfirmed) {
		$this->isConfirmed = $isConfirmed;

		return $this;
	}

	/**
	 * Get isConfirmed
	 *
	 * @return boolean
	 */
	public function getIsConfirmed() {
		return $this->isConfirmed;
	}

	/**
	 * Set confirmedAt
	 *
	 * @param \DateTime $confirmedAt
	 * @return Payment
	 */
	public function setConfirmedAt($confirmedAt) {
		$this->confirmedAt = $confirmedAt;

		return $this;
	}

	/**
	 * Get confirmedAt
	 *
	 * @return \DateTime
	 */
	public function getConfirmedAt() {
		return $this->confirmedAt;
	}

	/**
	 * Set confirmedBy
	 *
	 * @param string $confirmedBy
	 * @return Payment
	 */
	public function setConfirmedBy($confirmedBy) {
		$this->confirmedBy = $confirmedBy;

		return $this;
	}

	/**
	 * Get confirmedBy
	 *
	 * @return string
	 */
	public function getConfirmedBy() {
		return $this->confirmedBy;
	}
}
