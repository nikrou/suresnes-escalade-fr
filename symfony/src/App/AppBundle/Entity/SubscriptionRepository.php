<?php

namespace App\AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Ekino\WordpressBundle\Entity\User;

/**
 * SubscriptionRepository
 */
class SubscriptionRepository extends EntityRepository
{
	public function findByUser(User $user)
	{
		return $this->findByUserId($user->getId());
	}
}
