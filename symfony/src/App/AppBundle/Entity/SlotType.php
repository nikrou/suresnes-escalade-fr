<?php

namespace App\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SlotType
 *
 * @ORM\Table(name="wp_app_slot_type")
 * @ORM\Entity
 */
class SlotType {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=50)
	 * @Assert\NotBlank()
	 */
	private $title;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=500)
	 * @Assert\NotBlank()
	 */
	private $description;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_restricted", type="boolean", nullable=true)
	 */
	private $isRestricted;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return SlotType
	 */
	public function setTitle($title) {
		$this->title = $title;

		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return SlotType
	 */
	public function setDescription($description) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set isRestricted
	 *
	 * @param boolean $isRestricted
	 * @return SlotType
	 */
	public function setIsRestricted($isRestricted) {
		$this->isRestricted = $isRestricted;

		return $this;
	}

	/**
	 * Get isRestricted
	 *
	 * @return boolean
	 */
	public function getIsRestricted() {
		return $this->isRestricted;
	}

	public function __toString() {
		return sprintf('%s', $this->title);
	}
}
