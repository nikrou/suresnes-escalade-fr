<?php

namespace App\AppBundle\Listener;

use App\AppBundle\Event\SubscriptionEvent;
use App\AppBundle\Entity\Subscription;
use Symfony\Component\Security\Core\SecurityContextInterface;

class ActivationListener
{
    private $securityContext;

    public function __construct(SecurityContextInterface $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function onSubscriptionValidated(SubscriptionEvent $event)
    {
        $subscription = $event->getSubscription();

        if (null === $subscription->getActivatedAt()) {
            $subscription->setActivatedAt(new \DateTime());
        }

        if (null === $subscription->getActivatedBy() && $token = $this->securityContext->getToken()) {
            $subscription->setActivatedBy($token->getUsername());
        }
    }
}