<?php

namespace App\AppBundle\Listener;

use App\AppBundle\Event\SubscriptionEvent;
use App\AppBundle\Entity\Subscription;

class EmailListener
{
    private $mailer;
    private $twig;

    public function __construct($mailer, $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function onSubscriptionSubmitted(SubscriptionEvent $event)
    {
        $subscription = $event->getSubscription();

        $message = $this->createMessage($subscription)
            ->setSubject('Suresnes Escalade - Inscription en attente de validation '.$subscription)
            ->setBody($this->twig->render('AppBundle:Email:submitted.html.twig', array('subscription' => $subscription)))
        ;

        $this->mailer->send($message);
    }

    public function onSubscriptionValidated(SubscriptionEvent $event)
    {
        $subscription = $event->getSubscription();

        $message = $this->createMessage($subscription)
            ->setSubject('Suresnes Escalade - Validation de votre inscription '.$subscription)
            ->setBody($this->twig->render('AppBundle:Email:validated.html.twig', array('subscription' => $subscription)))
        ;
        $this->mailer->send($message);
    }

    /**
     * @param  Subscription $subscription
     * @return  \Swift_Message
     */
    private function createMessage(Subscription $subscription)
    {
        $message = \Swift_Message::newInstance()
            ->setFrom('inscription@suresnes-escalade.fr')
            ->setTo($subscription->getEmail())
            ->setBcc('inscription@suresnes-escalade.fr')
        ;

        if ($subscription->getParentEmail()) {
            $message->setCc($subscription->getParentEmail());
        }

        return $message;
    }
}