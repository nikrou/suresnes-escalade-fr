<?php

namespace App\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction($name)
    {
        var_dump($_SESSION);
        return array('name' => $name);
    }

    /**
     * @Route("/test")
     * @Template()
     */
    public function testAction()
    {
        var_dump($this->getService(''));
    }
}
