<?php

namespace App\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\AppBundle\Entity\Subscription;
use App\AppBundle\Form\SubscriptionType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Subscription controller.
 *
 * @Route("/creneaux")
 */
class SlotController extends Controller
{
    /**
     * Lists all Subscription entities.
     *
     * @Route("/", name="slots")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $season = $em->getRepository('AppBundle:Season')->findCurrent();
        $slotTypes = $em->getRepository('AppBundle:SlotType')->findAll();

        return array(
            'season' => $season,
            'slotTypes' => $slotTypes,
        );
    }

    /**
     * Lists all Subscription entities.
     *
     * @Route("/remplissage", name="slots_subscriptions")
     * @Method("GET")
     * @Template()
     */
    public function subscriptionsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $season = $em->getRepository('AppBundle:Season')->findCurrent();
        $slotTypes = $em->getRepository('AppBundle:SlotType')->findAll();

        return array(
            'season' => $season,
        );
    }
}
