<?php

namespace App\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\AppBundle\Entity\Profile;
use App\AppBundle\Form\ProfileType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Profile controller.
 *
 * @Route("/")
 */
class ProfileController extends Controller
{
    /**
     * Edits an existing Profile entity.
     *
     * @Route("/profile", name="app_profile")
     * @Template("AppBundle:Profile:edit.html.twig")
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $profile = $this->getPrivateProfile();

        $form = $this->createForm(new ProfileType(), $profile, array(
            'action' => $this->generateUrl('app_profile'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($profile);
            $em->flush();

            return $this->redirect($this->generateUrl('app_profile'));
        }

        return array(
            'entity'      => $profile,
            'edit_form'   => $form->createView(),
        );
    }

    /**
     * @return Profile
     */
    private function getPrivateProfile()
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $user = $this->get('security.context')->getToken()->getUser();

        // Fix reload user
        $user = $this->get('ekino.wordpress.manager.user')->find($user->getId());

        $em = $this->getDoctrine()->getManager();

        $profile = $em->getRepository('AppBundle:Profile')->findOneByUser($user);

        if (!$profile) {
            $profile = new Profile();
            $profile->setUser($user);
        }

        return $profile;
    }
}
