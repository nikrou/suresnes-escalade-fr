<?php

namespace App\AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\AppBundle\Entity\Subscription;
use App\AppBundle\Form;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Subscription controller.
 *
 * @Route("/inscription")
 */
class SubscriptionController extends Controller
{
    /**
     * Lists all Subscription entities.
     *
     * @Route("/", name="subscription")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getCurrentUser();
        $entities = $em->getRepository('AppBundle:Subscription')->findByUser($user);

        $currentSeason = $em->getRepository('AppBundle:Season')->findCurrent();
        $currentSubscription = $em->getRepository('AppBundle:Subscription')->findOneBy(array(
            'userId' => $user->getId(),
            'season' => $currentSeason,
        ));

        return array(
            'entities' => $entities,
            'season' => $currentSeason,
            'subscription' => $currentSubscription,
        );
    }

    /**
     * @Route("/profile", name="subscription_profile")
     * @Template()
     */
    public function profileAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->findPrivateSubscription();

        $form = $this->createForm(new Form\SubscriptionProfileType($entity), $entity, array(
            'action' => $this->generateUrl('subscription_profile'),
        ));
        $form->add('submit', 'submit', array('label' => 'Valider'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('subscription_profile'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/creneaux", name="subscription_slots")
     * @Template()
     */
    public function slotsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->findPrivateSubscription();

        $form = $this->createForm(new Form\SubscriptionSlotType($entity), $entity, array(
            'action' => $this->generateUrl('subscription_slots'),
        ));
        $form->add('submit', 'submit', array('label' => 'Valider'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('subscription_slots'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/certificat-medical", name="subscription_medical")
     * @Template()
     */
    public function medicalAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->findPrivateSubscription();

        $form = $this->createForm(new Form\SubscriptionMedicalType($entity), $entity, array(
            'action' => $this->generateUrl('subscription_medical'),
        ));
        $form->add('submit', 'submit', array('label' => 'Valider'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('subscription_medical'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }


    /**
     * @Route("/assurance", name="subscription_insurance")
     * @Template()
     */
    public function insuranceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->findPrivateSubscription();

        $form = $this->createForm(new Form\SubscriptionInsuranceType($entity), $entity, array(
            'action' => $this->generateUrl('subscription_insurance'),
        ));
        $form->add('submit', 'submit', array('label' => 'Valider'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('subscription_insurance'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }


    /**
     * @Route("/paiement", name="subscription_payment")
     * @Template()
     */
    public function paymentAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->findPrivateSubscription();

        return array(
            'entity' => $entity,
        );
    }


    /**
     * @Route("/validation", name="subscription_validation")
     * @Template()
     */
    public function validationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->findPrivateSubscription();

        return array(
            'entity' => $entity,
        );
    }

    private function getCurrentUser()
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        return $this->get('security.context')->getToken()->getUser();
    }

    private function findPrivateSubscription()
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $season = $em->getRepository('AppBundle:Season')->findCurrent();

        $user = $this->get('security.context')->getToken()->getUser();

        // Fix reload user
        $user = $this->get('ekino.wordpress.manager.user')->find($user->getId());

        $entity = $em->getRepository('AppBundle:Subscription')->findOneBy(array(
            'userId' => $user->getId(),
            'season' => $season,
        ));

        if (!$entity) {
            $entity = new Subscription();
            $entity->setUserId($user->getId());
            $entity->setSeason($season);
            $entity->setIsRenewal(false); // @TODO check in DB

            $em->persist($entity);
            $em->flush();
        }

        return $entity;
    }
}
