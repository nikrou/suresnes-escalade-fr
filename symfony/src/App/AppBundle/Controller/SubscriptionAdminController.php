<?php

namespace App\AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use App\AppBundle\AppEvents;
use App\AppBundle\Event\SubscriptionEvent;

class SubscriptionAdminController extends CRUDController
{
    public function printAction()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        return $this->render('AppBundle:SimpleSubscription:print.html.twig', array(
            'subscription' => $object,
        ));
    }

    public function voucherAction()
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        return $this->render('AppBundle:SimpleSubscription:voucher.html.twig', array(
            'subscription' => $object,
        ));
    }
}
